//
//  Application.swift
//  client
//
//  Created by Behrad Kazemi on 6/9/22.
//
import UIKit
import Foundation
import Platforms

public struct BooBool: Codable {
  public let boobool: Int
}
final class Application {
  static let shared = Application()
  var window: UIWindow?
  private init() {}
  
  func configureMainInterface(in window: UIWindow) {
    self.window = window
    let navigationController = MainNavigationController()
    
    let navigator = HomeNavigator(navigationController: navigationController, servicePackage: ServicePackage(dataAccess: UseCaseProviderIMP()))
    navigationController.viewControllers = [navigator.setup()]
    window.rootViewController = navigator.navigationController
    window.makeKeyAndVisible()
  }
  
  private func resetNotificationBadge() {
    UIApplication.shared.applicationIconBadgeNumber = 0
  }
  
}
