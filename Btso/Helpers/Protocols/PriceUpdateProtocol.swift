//
//  PriceUpdateProtocol.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
import UIKit
protocol PriceUpdateProtocol {
  func updatePrice(price: String)
}
