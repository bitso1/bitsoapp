//
//  BookDetailsViewController.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/17/22.
//

import UIKit

class BookDetailsViewController: ViewController {
  
  @IBOutlet weak var indicator: UIActivityIndicatorView!
  @IBOutlet weak var symbolLabel: UILabel!
  @IBOutlet weak var bidLabel: UILabel!
  @IBOutlet weak var askLabel: UILabel!
  @IBOutlet weak var lowLabel: UILabel!
  @IBOutlet weak var highLabel: UILabel!
  @IBOutlet weak var dayVolumeLabel: UILabel!
  @IBOutlet weak var spreadLabel: UILabel!
  @IBOutlet weak var updatedAtLabel: UILabel!
  var viewModel: BookDetailsViewModel?
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
  }
  
  func setupUI() {
    let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refresh))
    self.navigationItem.rightBarButtonItem = refreshButton
  }
  
  @objc func refresh() {
    self.update()
  }
  
  func setup(vm: BookDetailsViewModel) {
    self.viewModel = vm
    self.update()
  }
  
  func update() {
    indicator.startAnimating()
    self.viewModel?.loadData(completion: {[unowned self] _ in
      DispatchQueue.main.async { [unowned self] in
        indicator.stopAnimating()
        self.bidLabel.text = self.viewModel?.bid
        self.askLabel.text = self.viewModel?.ask
        self.lowLabel.text = self.viewModel?.low
        self.highLabel.text = self.viewModel?.high
        self.dayVolumeLabel.text = self.viewModel?.dayVolume
        self.spreadLabel.text = self.viewModel?.spread
        self.symbolLabel.text = self.viewModel?.symbol
        self.updatedAtLabel.text = self.viewModel?.updatedAt
      }
    })
  }
}
