//
//  BookDetailsViewModel.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
import Domain
class BookDetailsViewModel {
  
  let navigator: BookDetailsNavigator
  let useCases: BookDetailsUseCases
  let symbol: String
  var bid = ""
  var ask = ""
  var low = ""
  var high = ""
  var dayVolume = ""
  var updatedAt = ""
  var spread = ""
  init(symbol: String, navigator: BookDetailsNavigator, useCases: BookDetailsUseCases) {
    self.navigator = navigator
    self.useCases = useCases
    self.symbol = symbol
  }
  
  func loadData(completion: ((Error?) -> Void)? = nil) {
    useCases.getDetails(request: BookDetailIO.Request(bookSymbol: symbol), completion: { res in
      switch res {
      case .success(let details):
        self.bid = details.bid
        self.ask = details.ask
        self.low = details.low
        self.high = details.high
        self.dayVolume = details.volume
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.updatedAt = dateFormatter.string(from: details.created_at)
        if let bidDouble = Double(details.bid), let askDouble = Double(details.ask) {
          self.spread = String(bidDouble - askDouble)
        }
        completion?( nil)
      case .failure(let error):
        completion?(error)
      }
    }).resume()
  }
}
