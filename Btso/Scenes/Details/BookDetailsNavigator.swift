//
//  BookDetailsNavigator.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
final class BookDetailsNavigator: Navigator {
  func setup(symbol: String) -> BookDetailsViewController {
    let bookDetailsVC = BookDetailsViewController.initFromNib()
    bookDetailsVC.viewModel = BookDetailsViewModel(symbol: symbol, navigator: self, useCases: servicePackage.dataAccess.getBookDetailsUseCases())
    bookDetailsVC.viewModel?.loadData()
    return bookDetailsVC
  }
}
