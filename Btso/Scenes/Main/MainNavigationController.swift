//
//  MainNavigationController.swift
//  client
//
//  Created by Behrad Kazemi on 6/9/22.
//


import UIKit

class MainNavigationController: UINavigationController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupNavigationBarUI()
  }
  private func setupUI() {
  }
  private func setupNavigationBarUI() {
    UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for: .default)
    UINavigationBar.appearance().isTranslucent = true
  }

}

