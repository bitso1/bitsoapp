//
//  Navigator.swift
//  client
//
//  Created by Behrad Kazemi on 6/9/22.
//

import UIKit

class Navigator: NSObject {
  internal var navigationController: UINavigationController
  internal let servicePackage: ServicePackage
  
  init(navigationController: UINavigationController, servicePackage: ServicePackage) {
    self.navigationController = navigationController
    self.servicePackage = servicePackage
  }
  func pop() {
    navigationController.popViewController(animated: true)
  }
  func hideNavigationBar(_ isHidden: Bool) {
    navigationController.setNavigationBarHidden(isHidden, animated: false)
  }
  func dismiss() {
    navigationController.dismiss(animated: true, completion: nil)
  }
  func simpleAlert(title: String, message: String, actions: [UIAlertAction], preferredStyle: UIAlertController.Style = .actionSheet) -> UIAlertController {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
    for action in actions {
      alertController.addAction(action)
    }
    return alertController
  }

}


