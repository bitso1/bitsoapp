//
//  HomeViewModel.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/16/22.
//

import Foundation
import Domain
class HomeViewModel {
  
  let navigator: HomeNavigator
  let useCases: HomeUseCases
  var bookList: [BookCellViewModel] = []
  init(navigator: HomeNavigator, useCases: HomeUseCases) {
    self.navigator = navigator
    self.useCases = useCases
  }
  
  func loadData(completion: (([BookCellViewModel]?, Error?) -> Void)? = nil) {
    useCases.getBooks { [unowned self] res in
      switch res {
      case .success(let books):
        self.bookList = books.compactMap({ book in
          return BookCellViewModel(model: book, useCase: self.navigator.servicePackage.dataAccess.getBookDetailsUseCases())
        })
        completion?(self.bookList, nil)
      case .failure(let error):
        completion?(nil, error)
      }
    }.resume()
  }
}
