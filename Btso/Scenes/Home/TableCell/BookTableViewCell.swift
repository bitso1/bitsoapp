//
//  BookTableViewCell.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/16/22.
//

import UIKit

class BookTableViewCell: UITableViewCell {

  @IBOutlet weak var symbolLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var indicator: UIActivityIndicatorView!
  var viewModel: BookCellViewModel?
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
  func setViewModel(vm: BookCellViewModel) {
    viewModel = vm
    symbolLabel.text = vm.symbol
    priceLabel.text = vm.lastPrice
    viewModel?.priceDelegate = self
    indicator.startAnimating()
    viewModel?.updatePrice()
  }
}

extension BookTableViewCell: PriceUpdateProtocol {
  func updatePrice(price: String) {
    DispatchQueue.main.async {
      self.priceLabel.text = price
      self.indicator.stopAnimating()
    }
  }
}
